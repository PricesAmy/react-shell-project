import React, { Component } from "react";
import "./App.less";
import { DatePicker, Button } from "antd";
class App extends Component {
  render() {
    return (
      <div className="App">
        <Button>button</Button>
        <DatePicker />
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
      </div>
    );
  }
}

export default App;
